//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.zuitt.example;

public class Car {
    private String name;
    private String brand;
    private int yearOfMake;
    private Driver driver;

    public Car() {
        this.yearOfMake = 2000;
        this.driver = new Driver("Alejandro");
    }

    public Car(String name, String brand, int yearOfMake) {
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("Alejandro");
    }

    public String getName() {
        return this.name;
    }

    public String getBrand() {
        return this.brand;
    }

    public int getYearOfMake() {
        return this.yearOfMake;
    }

    public String getDriverName() {
        return this.driver.getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake) {
        if (yearOfMake <= 2023) {
            this.yearOfMake = yearOfMake;
        }

    }

    public void setDriver(String driver) {
        this.driver.setName(driver);
    }

    public void drive() {
        System.out.println("This car is running. ");
    }
}
