//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import com.zuitt.example.Car;
import com.zuitt.example.Dog;
import java.io.PrintStream;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        Car myCar = new Car();
        myCar.drive();
        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2025);
        System.out.println("Car name: " + myCar.getName());
        System.out.println("Car brand: " + myCar.getBrand());
        System.out.println("Car year of make: " + myCar.getYearOfMake());
        System.out.println("Driver name: " + myCar.getDriverName());
        myCar.setDriver("John Smith");
        System.out.println("Driver name: " + myCar.getDriverName());
        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        myPet.call();
        PrintStream var10000 = System.out;
        String var10001 = myPet.getName();
        var10000.println("Pet name: " + var10001 + ". Breed: " + myPet.getBreed() + ". Color: " + myPet.getColor());
    }
}
